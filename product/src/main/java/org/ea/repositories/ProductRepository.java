package org.ea.repositories;

import org.ea.entities.Product;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class ProductRepository {

    @Inject
    EntityManager em;

    @Transactional
    public void createProduct(Product p){
        em.persist(p);
    }

    @Transactional
    public void deleteProduct(Product p){
        em.remove(p);
    }

    @Transactional
    public List<Product> listProduct(){
        return em.createQuery("select p from Product p").getResultList();
    }

    @Transactional
    public Product findProduct(Long id){
        return em.find(Product.class, id);
    }

    @Transactional
    public void updateProduct(Product p){
        em.merge(p);
    }
}
