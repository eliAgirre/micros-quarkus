package org.ea.controllers;

import org.ea.entities.Product;
import org.ea.repositories.ProductRepository;

import javax.inject.Inject;
import javax.ws.rs.*;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.util.List;

@Path(ProductController.BASE_MAPPING)
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ProductController {

    static final String BASE_MAPPING = "/api/product";

    static final String PATH_ID = "/{id}";

    static final String PATH_PARAM = "id";

    @Inject
    ProductRepository productRep;

    @GET
    public List<Product> list(){
        return productRep.listProduct();
    }

    @GET
    @Path(ProductController.PATH_ID)
    public Product getById(@PathParam(PATH_PARAM) Long id) {
        return productRep.findProduct(id);
    }

    @POST
    public Response add(Product product){
        productRep.createProduct(product);
        return Response.ok().build();
    }

    @PUT
    public Response update(Product pParam) {
        Product product = productRep.findProduct(pParam.getId());
        product.setCode(pParam.getCode());
        product.setName(pParam.getName());
        product.setDescription(pParam.getDescription());
        productRep.updateProduct(product);
        return Response.ok().build();
    }

    @DELETE
    @Path(ProductController.PATH_ID)
    public Response delete(@PathParam(PATH_PARAM) Long id){
        productRep.deleteProduct(productRep.findProduct(id));
        return Response.ok().build();
    }

//    @DELETE
//    public Response delete(Product product){
//        productRep.deleteProduct(product);
//        return Response.ok().build();
//    }
}
