# code-with-quarkus

Para arrancar la aplicación en modo desarrollo se ha utilizado el siguiente comando:

```shell script
./mvnw compile quarkus:dev
```

En el navegador escribimos la url `http://localhost:8082/customer.html` para ver el formulario y para ver Open API iremos a la url `http://localhost:8082/q/swagger/`.

------------------------------------------------------------------------------------------------------------------------

Para crear una imagen nativa vamos a usar el comando `./mvnw package -Pnative`.

Para comprobar que todo ha ido bien haremos un `ls -la` dentro de la carpeta `target` para ver si ha generado el archivo `product-1.0.0-SNAPSHOT-runner.jar`.

Para comprobar si funciona la aplicación nativa escribimos `./target/product-1.0.0-SNAPSHOT-runner`.

Para crear una imagen utilizaremos el fichero `Dockerfile.native`.

Primero añadiremos el puerto con el comando `EXPOSE 8082`.

Para crear la imagen usaremos el comando `docker build -f src/main/docker/Dockerfile.native -t quarkus/product .`.

Para arrancar el contenedor usaremos el comando `docker run -i -p 8082:8080 quarkus/product` o `docker run -i --rm -p 8082:8080 quarkus/product`.

------------------------------------------------------------------------------------------------------------------------

Si no se puede realizar la imagen docker con la imagen nativa se usará el comando `./mvnw package`.

Para comprobar si funciona la imagen escribimos `java -jar target/quarkus-app/quarkus-run.jar`.

Primero vamos a eliminar algunos contenedores y las imágenes que se han creado para este microservicio:

```shell
docker ps -a

docker rm <nombreContenedor>

docker image ls

docker rmi <nombreImage>
```

Para crear una imagen utilizaremos el fichero `Dockerfile.jvm`.

Primero añadiremos el puerto con el comando `EXPOSE 8082`.

Para crear la imagen usaremos el comando `docker build -f src/main/docker/Dockerfile.jvm -t quarkus/product-jvm .`.

Para arrancar el contenedor usaremos el comando `docker run -d --name product -p 8082:8082 quarkus/product-jvm`.

El parámetro `-d` es para que ejecute en background.

Y el parámetro `--name` sirve para darle un nombre específico al contenedor.

Mediante el comando `docker ps -a` podemos listar los contenedores. Se puede ver con el `status` up y con el nombre que le hemos indicado.

Ahora ya se está ejecutando como un contenedor e iremos al navegador apuntando a la url `0.0.0.0:8082/q/swagger` y ahí se carga el interfaz de Swagger para probar.

Primero probaremos añadiendo varios productos con el método `POST`:

```json
{
    "code": "01",
    "name": "Cuenta de ahorros",
    "description": "Cuenta de ahorro"    
}
```

```json
{
    "code": "02",
    "name": "Tarjetas de credito",
    "description": "Tarjeta credito personal"    
}
```


--------------------------------------------------------------------------------------------------------------------

This project uses Quarkus, the Supersonic Subatomic Java Framework.

If you want to learn more about Quarkus, please visit its website: https://quarkus.io/ .

## Running the application in dev mode

You can run your application in dev mode that enables live coding using:
```shell script
./mvnw compile quarkus:dev
```

> **_NOTE:_**  Quarkus now ships with a Dev UI, which is available in dev mode only at http://localhost:8080/q/dev/.

## Packaging and running the application

The application can be packaged using:
```shell script
./mvnw package
```
It produces the `quarkus-run.jar` file in the `target/quarkus-app/` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `target/quarkus-app/lib/` directory.

The application is now runnable using `java -jar target/quarkus-app/quarkus-run.jar`.

If you want to build an _über-jar_, execute the following command:
```shell script
./mvnw package -Dquarkus.package.type=uber-jar
```

The application, packaged as an _über-jar_, is now runnable using `java -jar target/*-runner.jar`.

## Creating a native executable

You can create a native executable using: 
```shell script
./mvnw package -Pnative
```

Or, if you don't have GraalVM installed, you can run the native executable build in a container using: 
```shell script
./mvnw package -Pnative -Dquarkus.native.container-build=true
```

You can then execute your native executable with: `./target/code-with-quarkus-1.0.0-SNAPSHOT-runner`

If you want to learn more about building native executables, please consult https://quarkus.io/guides/maven-tooling.

## Related Guides

- Hibernate ORM ([guide](https://quarkus.io/guides/hibernate-orm)): Define your persistent model with Hibernate ORM and JPA

## Provided Code

### Hibernate ORM

Create your first JPA entity

[Related guide section...](https://quarkus.io/guides/hibernate-orm)



### RESTEasy JAX-RS

Easily start your RESTful Web Services

[Related guide section...](https://quarkus.io/guides/getting-started#the-jax-rs-resources)
