# Microservicio customer

Se ha creado el microservicio `Customer` con Quarkus con la extensión de Web `RestEasy Classic Jackson` y `SmallRye OpenAPI`.

En Data `Hibernate ORM` y se ha añadido el driver base de datos H2.

Se ha creado el paquete `entities` para la clase `Customer` con los siguientes atributos:


```java
@Entity
@Data
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String code;
    private String accountNumber;
    private String name;
    private String surname;
    private String phone;
    private String address;
    private List<String> products;
} 
```

Se ha creado para ello un repositorio:

```java
@ApplicationScoped
public class CustomerRepository {

    @Inject
    EntityManager em;

    @Transactional
    public void createCustomer(Customer customer){
        em.persist(customer);
    }

    @Transactional
    public void deleteCustomer(Customer customer){
        em.remove(customer);
    }

    @Transactional
    public List<Customer> listCustomer(){
        return em.createQuery("select c from Customer c").getResultList();
    }

}
```

Después se ha creado un controlador:

```java
@Path(CustomerController.BASE_MAPPING)
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CustomerController {

    static final String BASE_MAPPING = "/api/customer";

    @Inject
    CustomerRepository customerRepository;

    @GET
    public List<Customer> list(){
        return customerRepository.listCustomer();
    }

    @POST
    public Response add(Customer customer){
        customerRepository.createCustomer(customer);
        return Response.ok().build();
    }

    @DELETE
    public Response delete(Customer customer){
        customerRepository.deleteCustomer(customer);
        return Response.ok().build();
    }
}
```

Luego se ha creado un object mapper:

```java
@Singleton
public class CustomObjectMapper implements ObjectMapperCustomizer {

    @Override
    public void customize(ObjectMapper objectMapper) {
        // To suppress serializing properties with null values
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }
}
```

Se crea un formulario y una tabla para ver los datos de los clientes en html.

Y se ha añadido las propiedades de la base de datos en memoria h2 y open api (swagger).

Para arrancar la aplicación en modo desarrollo se ha utilizado el siguiente comando:

```shell script
./mvnw compile quarkus:dev
```

En el navegador escribimos la url `http://localhost:8081/customer.html` para ver el formulario y para ver Open API iremos a la url `http://localhost:8081/q/swagger/`.

------------------------------------------------------------------------------------------------------------------------

## Crear una imagen nativa - Linux

Para crear una imagen nativa vamos a usar el comando `./mvnw package -Pnative`.

Para comprobar que todo ha ido bien haremos un `ls -la` dentro de la carpeta `target` para ver si ha generado el archivo `customer-1.0.0-SNAPSHOT-runner.jar`.

Para comprobar si funciona la aplicación nativa escribimos `./target/customer-1.0.0-SNAPSHOT-runner`.

Para crear una imagen utilizaremos el fichero `Dockerfile.native`.

Primero añadiremos el puerto con el comando `EXPOSE 8081`.

Para crear la imagen usaremos el comando `docker build -f src/main/docker/Dockerfile.native -t quarkus/customer .`.

Para arrancar el contenedor usaremos el comando `docker run -i -p 8081:8080 quarkus/customer` o `docker run -i --rm -p 8081:8080 quarkus/customer`.

------------------------------------------------------------------------------------------------------------------------

## Crear una imagen - Linux

Si no se puede realizar la imagen docker con la imagen nativa se usará el comando `./mvnw package`.

Para comprobar si funciona la imagen escribimos `java -jar target/quarkus-app/quarkus-run.jar`.

Primero vamos a eliminar algunos contenedores y las imágenes que se han creado para este microservicio:

```shell
docker ps -a

docker rm <nombreContenedor>

docker image ls

docker rmi <nombreImage>
```

Para crear una imagen utilizaremos el fichero `Dockerfile.jvm`.

Primero añadiremos el puerto con el comando `EXPOSE 8081`.

Para crear la imagen usaremos el comando `docker build -f src/main/docker/Dockerfile.jvm -t quarkus/customer-jvm .`.

Para arrancar el contenedor usaremos el comando `docker run -i --rm -p 8081:8081 quarkus/customer-jvm`.

Para arrancar el contenedor usaremos el comando `docker run -d --name customer -p 8082:8082 quarkus/customer-jvm`.

El parámetro `-d` es para que ejecute en background.

Y el parámetro `--name` sirve para darle un nombre específico al contenedor.

Mediante el comando `docker ps -a` podemos listar los contenedores. Se puede ver con el `status` up y con el nombre que le hemos indicado.

Ahora ya se está ejecutando como un contenedor e iremos al navegador apuntando a la url `0.0.0.0:8082/q/swagger` y ahí se carga la interfaz de Swagger para probar.

Primero probaremos añadiendo un customer con 2 productos mediante el método `POST`:

```json
{
    "accountNumber": "2323256568974",
    "address": "Calle ca 23",
    "code": "01",
    "name": "primer cliente de prueba",
    "phone": "683270000",
    "products": [
        {
            "product": 1
        },
        {
            "product": 2
        }
    ],
    "surname": "altosano"
}
```

------------------------------------------------------------------------------------------------------------------------

## Crear una imagen nativa - Windows

Vamos a ir al `Inicio de Windows` y vamos a buscar la palabra `native` para que nos inicie una consola nativa de 64 bits.

Vamos a hacer un par de comprobaciones para saber si la consola es nativa o no.

Vamos a escribir `echo %GRAALVM_HOME%` y vemos que está apuntando correctamente.

Y la segunda comprobación es `native-image --version` y deberíamos ver una salida parecida a `GraalVM 22.3.1 Java 11 CE`.

Hay que copiar y pegar en la consola la ruta del proyecto, y entrar en esa ruta.

Hacemos un `dir` para ver todo lo que contiene.

Y vamos a ejecutar el `mvnm` con el perfil `native` con el siguiente comando:

```script
mvnw package -Pnative
```

------------------------------------------------------------------------------------------------------------------------

## Crear una imagen - Windows

Usaremos el comando `./mvnw package` para crear una imagen.

Para comprobar si funciona la imagen escribimos `java -jar target/quarkus-app/quarkus-run.jar`.