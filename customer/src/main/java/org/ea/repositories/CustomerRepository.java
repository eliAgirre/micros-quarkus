package org.ea.repositories;

import org.ea.entities.Customer;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@ApplicationScoped
public class CustomerRepository {

    @Inject
    EntityManager em;

    @Transactional
    public void createCustomer(Customer customer){
        em.persist(customer);
    }

    @Transactional
    public void deleteCustomer(Customer customer){
        em.remove(em.contains(customer) ? customer : em.merge(customer));
    }

    @Transactional
    public List<Customer> listCustomer(){
        return em.createQuery("select c from Customer c").getResultList();
    }

    @Transactional
    public Customer findCustomer(Long id){
        return em.find(Customer.class, id);
    }

    @Transactional
    public void updateCustomer(Customer customer){
        em.merge(customer);
    }
}
