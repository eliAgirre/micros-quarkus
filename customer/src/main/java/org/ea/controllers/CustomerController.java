package org.ea.controllers;

import org.ea.entities.Customer;
import org.ea.repositories.CustomerRepository;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import javax.inject.Inject;
import javax.ws.rs.core.Response;
import java.util.List;

@Path(CustomerController.BASE_MAPPING)
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class CustomerController {

    static final String BASE_MAPPING = "/api/customer";

    static final String PATH_ID = "/{id}";

    static final String PARAM_ID = "id";

    @Inject
    CustomerRepository customerRepo;

    @GET
    public List<Customer> list(){
        return customerRepo.listCustomer();
    }

    @GET
    @Path(CustomerController.PATH_ID)
    public Customer getById(@PathParam(PARAM_ID) Long id) {
        return customerRepo.findCustomer(id);
    }

//    @POST
//    public Response add(Customer customer){
//        customerRepo.createCustomer(customer);
//        return Response.ok().build();
//    }

    @POST
    public Response add(Customer paramCustomer) {
        paramCustomer.getProducts().forEach(customer-> customer.setCustomer(paramCustomer));
        customerRepo.createCustomer(paramCustomer);
        return Response.ok().build();
    }

    @PUT
    public Response update(Customer paramCustomer) {
        Customer customer = customerRepo.findCustomer(paramCustomer.getId());
        customer.setCode(paramCustomer.getCode());
        customer.setAccountNumber(paramCustomer.getAccountNumber());
        customer.setSurname(paramCustomer.getSurname());
        customer.setPhone(paramCustomer.getPhone());
        customer.setAddress(paramCustomer.getAddress());
        customer.setProducts(paramCustomer.getProducts());
        customerRepo.updateCustomer(customer);
        return Response.ok().build();
    }

    @DELETE
    @Path(CustomerController.PATH_ID)
    public Response delete(@PathParam(PARAM_ID) Long id) {
        Customer customer = customerRepo.findCustomer(id);
        customerRepo.deleteCustomer(customer);
        return Response.ok().build();
    }

//    @DELETE
//    public Response delete(Customer customer){
//        customerRepo.deleteCustomer(customer);
//        return Response.ok().build();
//    }
}